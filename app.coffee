prfApp = undefined
prfApp = angular.module("prfApp", [
  "ngRoute"
  "ui.directives"
  "ui.filters"
  "highcharts-ng"
  "ngCookies"
])
prfApp.controller "prfController", [
  "$scope"
  "$cookies"
  "$location"
  "$rootScope"
  ($scope, $cookies, $location, $rootScope) ->
    $scope.login = false
    $rootScope.loggedIn = $scope.login
    $rootScope.disqusShortName = "prfantasytest"
    $scope.loginStyle = display: "none"
    $scope.loginEmail = ""
    $scope.loginPass = ""
    $scope.loginModal = false
    $scope.loginLabel = "Affiliate Log In"
    $scope.showLogin = ->
      $scope.loginModal = true
      $scope.loginStyle = display: "block"
      return

    $scope.hideLogin = ->
      $scope.loginModal = false
      $scope.loginStyle = display: "none"
      return

    $scope.execLogin = ->
      $scope.hideLogin()
      console.log "Email: " + $scope.loginEmail + " Pass: " + $scope.loginPass
      $scope.loginPass = ""
      $scope.login = true
      $rootScope.loggedIn = $scope.login
      $scope.loginLabel = "Log Out"
      $location.url "/affiliate"
      return

    $scope.loginBtnPress = ->
      if $scope.login
        $scope.login = false
        $scope.loginLabel = "Affiliate Log In"
        $rootScope.loggedIn = $scope.login
        if $location.path() is "/affiliate"
          $scope.loginModal = true
          $location.url "/racedata"
      else
        $scope.showLogin()
      return

    prfSession = $cookies.prfSession
    console.log "sid:" + prfSession
    console.log $scope
]
prfApp.config [
  "$routeProvider"
  "$locationProvider"
  ($routeProvider, $locationProvider) ->
    $routeProvider.when("/",
      templateUrl: "app/Landing/landing.html"
      controller: "landingController"
    ).when("/about",
      templateUrl: "app/About/about.html"
    ).when("/contact",
      templateUrl: "app/Contact/contact.html"
      controller: "contactController"
    ).when("/privacy",
      templateUrl: "app/Privacy/privacy.html"
    ).when("/tos",
      templateUrl: "app/ToS/tos.html"
    ).when("/racedata",
      templateUrl: "app/RaceData/raceData.html"
      controller: "raceDataController"
      reloadOnSearch: false
    ).when("/affiliate",
      templateUrl: "app/Affiliate/affiliate.html"
      controller: "affiliateController"
    ).when("/login",
      templateUrl: "app/Login/login.html"
      controller: "loginController"
    ).otherwise redirectTo: "/"
    $locationProvider.hashPrefix "!"
]
prfApp.run ($rootScope, $location) ->
  $rootScope.$on "$routeChangeStart", (event, next, current) ->
    console.log next.templateUrl
    $location.path "/login"  unless $rootScope.loggedIn  if next.templateUrl is "app/Affiliate/affiliate.html"
    return

  return