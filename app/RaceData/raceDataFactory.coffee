prfApp.factory "raceFactory", [
  "$http"
  ($http) ->
    getData_Races = ->
      
      #$http.get("http://www.pitrho.tv/api/fantasy/races", {'cache': false});
      $http.get "json/races.json",
        cache: false


    getData_RaceCharts = (raceID, chartID) ->
      
      #return $http.get("http://www.pitrho.tv/api/fantasy/" + raceID + "/" + chartID, {'cache': true});
      if chartID is 1
        $http.get "json/pointsCostTradeoff.json",
          cache: true

      else if chartID is 2
        $http.get "json/driverAttractiveness.json",
          cache: true

      else if chartID is 4
        $http.get "json/expectedFantasyPoints.json",
          cache: true

      else if chartID is 6
        $http.get "json/winProbability.json",
          cache: true


    getData_Charts = ->
      
      #return $http.get("http://www.pitrho.tv/api/fantasy/" + raceID + "/" + chartID, {'cache': true});
      $http.get "json/charts.json",
        cache: false


    return (
      getData_Races: getData_Races
      getData_RaceCharts: getData_RaceCharts
      getData_Charts: getData_Charts
    )
]