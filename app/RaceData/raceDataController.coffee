prfApp.controller "raceDataController", [
  "$scope"
  "$filter"
  "$window"
  "$location"
  "$routeParams"
  "raceFactory"
  ($scope, $filter, $window, $location, $routeParams, raceFactory) ->
    
    #init
    $scope.races = []
    $scope.charts = [
      {
        id: 1
        name: "Points Cost Trade-off"
      }
      {
        id: 2
        name: "Most Attactive Driver Selections"
      }
      {
        id: 3
        name: "Most Attractive Five-Man Groups"
      }
      {
        id: 4
        name: "Expected Fantasy Points"
      }
      {
        id: 5
        name: "Driver Projections"
      }
      {
        id: 6
        name: "Win Probability"
      }
    ]
    $scope.queryRace = null
    $scope.queryChart = null
    $scope.disqusIdentifier = null
    $scope.disqusUrl = null
    $scope.disqusTitle = null
    $scope.disqusReady = false
    $scope.tableWinProb = []
    
    #Load Races
    raceFactory.getData_Races().then (promise) ->
      $scope.races = promise.data
      $scope.parseRaceParams()
      return

    
    #App's Query Params Logic
    $scope.$on "$routeChangeSuccess", ->
      $scope.parseRaceParams()
      return

    $scope.$on "$routeUpdate", ->
      $scope.parseRaceParams()
      return

    
    #App's Query Params Function
    $scope.parseRaceParams = ->
      console.log $location.search()
      if angular.isUndefined($location.search().race)
        $scope.queryRace = null
        $scope.queryChart = null
        $scope.selectedRace = null
        $scope.selectedChart = null
      else
        $scope.queryRace = $location.search().race
        if angular.isUndefined($location.search().chart)
          $scope.queryChart = 1
          $location.search "chart", 1
        else
          $scope.queryChart = $location.search().chart
        i = 0

        while i < $scope.races.length
          if $scope.races[i]["id"] is $scope.queryRace
            $scope.selectedRace = $scope.races[i]
            break
          i++
        i = 0

        while i < $scope.charts.length
          if $scope.charts[i]["id"] is $scope.queryChart
            $scope.selectedChart = $scope.charts[i]
            $scope.chartConfig = $scope.selectedChart.chartConfig
            break
          i++
        unless $scope.disqusIdentifier is "r" + $scope.queryRace
          $scope.disqusIdentifier = "r" + $scope.queryRace
          $scope.disqusUrl = $location.absUrl()
          $scope.disqusTitle = $scope.selectedRace.name
          if $window.DISQUS
            $window.DISQUS.reset
              reload: true
              config: ->
                @page.identifier = $scope.disqusIdentifier
                @page.url = $scope.disqusUrl
                @page.title = $scope.disqusTitle
                return

        $scope.disqusReady = true
        $scope.getChartData()
      $scope.hasActiveRace = (if $scope.queryRace? then true else false)
      return

    $scope.selectedYear = ""
    $scope.selectedSeries = ""
    $scope.selectedRace = ""
    $scope.chartIsActive = (chart) ->
      (if $scope.queryChart is chart.id then true else false)

    $scope.selectedRaceChange = ->
      $location.search "race", (if $scope.selectedRace? then $scope.selectedRace.id else null)
      return

    $scope.selectedChartChange = (chart) ->
      $scope.selectedChart = chart
      $location.search "chart", $scope.selectedChart.id
      return

    $scope.getChartData = ->
      p = raceFactory.getData_RaceCharts($scope.queryRace, $scope.queryChart)
      return  if angular.isUndefined(p)
      p.then (promise) ->
        if $scope.queryChart is 1
          $scope.pctoConfig.series[0].data = $scope.linRegLine(promise.data) #TODO: needs to be done on server
          $scope.pctoConfig.series[1].data = promise.data
        else if $scope.queryChart is 2
          $scope.madsConfig.series[0].data = promise.data
        else if $scope.queryChart is 4
          $scope.exfpConfig.series[0].data = $scope.makeSeries(promise.data, "ptsFinish")
          $scope.exfpConfig.series[1].data = $scope.makeSeries(promise.data, "ptsLI")
          $scope.exfpConfig.series[2].data = $scope.makeSeries(promise.data, "ptsFI")
          $scope.exfpConfig.series[3].data = $scope.makeSeries(promise.data, "ptsDpos")
        else if $scope.queryChart is 6
          $scope.tableWinProb = promise.data
          console.log $scope.tableWinProb
        return

      return

    
    #raceFactory.getData_RaceCharts(0,0).then(function(promise){
    #		$scope.chartConfig.series[0].data = promise.data;
    #	});
    $scope.raceSort = "fa fa-calendar"
    $scope.raceSortFilter = "date"
    $scope.sortRace = ->
      if $scope.raceSort is "fa fa-calendar"
        $scope.raceSort = "fa fa-sort-alpha-asc"
        $scope.raceSortFilter = "name"
      else
        $scope.raceSort = "fa fa-calendar"
        $scope.raceSortFilter = "date"
      return

    $scope.chartConfig = {}
    $scope.raceDate = (date) ->
      $filter("date") date, "dd MMM"

    $scope.pctoConfig =
      options:
        chart:
          type: "scatter"

        tooltip:
          style:
            padding: 10
            fontWeight: "bold"

        tooltip:
          formatter: ->
            "<b>Driver:</b> " + @point.name + "<br/>" + "<b>Projected Points:</b> " + $filter("number")(@point.y, 2) + "<br/>" + "<b>Fantasy Cost:</b> " + $filter("number")(@point.y, 2) + "<br/>" + "<b>Value Over Cost:</b> " + $filter("number")(@point.voc, 2)

        exporting:
          buttons:
            contextButton:
              enabled: false

      
      #The below properties are watched separately for changes.
      
      #Series object (optional) - a list of series using normal highcharts series options.
      series: [
        {
          type: "line"
          name: "Regression Line"
          data: []
          marker:
            enabled: false

          states:
            hover:
              lineWidth: 0

          enableMouseTracking: false
        }
        {
          type: "scatter"
          name: "Driver's Point to Cost Tradeoff"
          data: []
          dataLabels:
            enabled: true
            borderRadius: 5
            backgroundColor: "rgba(0, 0, 0, 0.1)"
            y: 12
            x: -16
            formatter: ->
              @point.init

          marker:
            symbol: "circle"
            lineColor: "#000000"
            lineWidth: 1
            radius: 4
        }
      ]
      title:
        text: "Points Cost Trade-off"

      loading: false
      size:
        height: 800

    $scope.madsConfig =
      options:
        chart:
          type: "bar"

        tooltip:
          style:
            padding: 10
            fontWeight: "bold"

        tooltip:
          formatter: ->
            "<b>Driver: " + @point.name + "</b><br/>Attractiveness: " + $filter("number")(@point.y, 1)

        xAxis:
          type: "category"
          labels:
            step: 1

        exporting:
          buttons:
            contextButton:
              enabled: false

      
      #The below properties are watched separately for changes.
      
      #Series object (optional) - a list of series using normal highcharts series options.
      series: [
        name: "Attractiveness"
        data: []
      ]
      
      #Title configuration (optional)
      title:
        text: "Most Attractive Driver Selections"

      
      #Boolean to control showng loading status on chart (optional)
      #Could be a string if you want to show specific loading text.
      loading: false
      
      #Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
      #properties currentMin and currentMax provied 2-way binding to the chart's maximimum and minimum
      xAxis:
        type: "category"
        labels:
          step: 1

      
      #function (optional)
      func: (chart) ->

      
      #setup some logic for the chart
      size:
        height: 800

    $scope.exfpConfig =
      options:
        chart:
          type: "bar"

        tooltip:
          style:
            padding: 10
            fontWeight: "bold"

        xAxis:
          type: "category"
          labels:
            step: 1

        yAxis:
          title:
            text: null

        plotOptions:
          series:
            stacking: "normal"
            states:
              hover: {}

        tooltip:
          formatter: ->
            "<b>Driver:</b> " + @point.name + "<br/>" + "<b>" + @point.series.name + ":</b> " + $filter("number")(@point.y, 1)

        exporting:
          buttons:
            contextButton:
              enabled: false

      series: [
        {
          name: "Finish Points"
          data: []
          color: "rgb(31,119,180)"
        }
        {
          name: "LI Points"
          data: []
          color: "rgb(44,160,44)"
        }
        {
          name: "FI Points"
          data: []
          color: "rgb(214,39,40)"
        }
        {
          name: "Dpos Points"
          data: []
          color: "rgb(255,127,14)"
        }
      ]
      title:
        text: "Expected Fantasy Points"

      loading: false
      size:
        height: 800

    $scope.makeSeries = (data, key) ->
      newList = []
      dataLen = data.length
      v = 0

      while v < dataLen
        valName = data[v].name
        valKey = data[v][key]
        newList.push
          name: valName
          y: valKey

        v++
      newList

    $scope.linRegLine = (values) ->
      xSum = 0
      ySum = 0
      xySum = 0
      xxSum = 0
      cnt = 0
      x = 0
      y = 0
      valuesLen = values.length
      
      #Catch If Empty
      return []  if valuesLen is 0
      
      #Calculate LinReg
      v = 0

      while v < valuesLen
        x = values[v].x
        y = values[v].y
        xSum += x
        ySum += y
        xxSum += x * x
        xySum += x * y
        cnt++
        v++
      
      #Calculate Lin Slope
      m = (cnt * xySum - xSum * ySum) / (cnt * xxSum - xSum * xSum)
      b = (ySum / cnt - (m * xSum) / cnt)
      
      #Create Lin Reg
      xyResults = []
      v = 0

      while v < valuesLen
        x = values[v].x
        y = x * m + b
        xyResults.push
          x: x
          y: y

        v++
      xyResults
]